/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voiceip;

import java.awt.Color;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;
import webphone.*;
/**
 *
 * @author thewa
 */
public class MainPanel extends javax.swing.JFrame {

    public  String myNm = "9";
    public boolean callincoming,callincomingmode;//avisa si la llamada sera contestda y sis esta entrando una llamada
    public boolean calloutcoming,callincomoutgmode;//avisa si la llamada fue contestda y si se esta realizando una llamada
    private String Ip,incomingIp;
    public webphone wobj = new webphone();
    public SIPNotifications sipnotifications = new SIPNotifications(wobj);



 
    private static AudioFormat getaudioformat(){
        float rateSample = 8000.8F;
        int sampleBits = 16;
        int channel = 2;
        boolean signed = true;
        boolean bigEnd = false;
        return new AudioFormat(rateSample,sampleBits,channel,signed,bigEnd);
     
    }
    
    public TargetDataLine audio_in;
    public SourceDataLine audio_out;
    /**
     * Creates new form MainPanel
     */
    public MainPanel() {
        
            initComponents();
            jLabel2.setText(String.valueOf(myNm));

//Signaling();

    }

    public void Signaling(){
        Runnable r = new HiloSiganls(this);
        Thread t = new Thread(r);
        t.start();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnStart = new javax.swing.JButton();
        btnDetiene = new javax.swing.JButton();
        PanelAviso = new javax.swing.JPanel();
        Label = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        IpSet = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        btnStart.setText("Empieza");
        btnStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStartActionPerformed(evt);
            }
        });

        btnDetiene.setText("Detiene");
        btnDetiene.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDetieneActionPerformed(evt);
            }
        });

        PanelAviso.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setText("jLabel2");

        javax.swing.GroupLayout PanelAvisoLayout = new javax.swing.GroupLayout(PanelAviso);
        PanelAviso.setLayout(PanelAvisoLayout);
        PanelAvisoLayout.setHorizontalGroup(
            PanelAvisoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelAvisoLayout.createSequentialGroup()
                .addGap(122, 122, 122)
                .addComponent(Label)
                .addGap(45, 45, 45)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PanelAvisoLayout.setVerticalGroup(
            PanelAvisoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelAvisoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(PanelAvisoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(Label))
                .addGap(19, 19, 19))
        );

        jLabel1.setFont(new java.awt.Font("Agency FB", 0, 18)); // NOI18N
        jLabel1.setText("Telefono");

        jLabel4.setText("Numero telefono");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(170, 170, 170)
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(149, 149, 149)
                .addComponent(btnStart)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 67, Short.MAX_VALUE)
                .addComponent(btnDetiene)
                .addGap(35, 35, 35))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jLabel4)
                .addGap(60, 60, 60)
                .addComponent(IpSet)
                .addContainerGap())
            .addComponent(PanelAviso, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(9, 9, 9)
                .addComponent(PanelAviso, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(IpSet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 131, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnStart)
                    .addComponent(btnDetiene))
                .addGap(285, 285, 285))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStartActionPerformed
           sipnotifications.Start();
           //Set parameters (replace uppercase words)
           wobj.API_SetParameter("serveraddress", "192.168.43.12");
           wobj.API_SetParameter("username", "101");
           wobj.API_SetParameter("password", "test01");
           wobj.API_SetParameter("vad", "4");
           wobj.API_SetParameter("aec", "1");
           wobj.API_SetParameter("aqtest", "1");
           wobj.API_SetParameter("loglevel", "5");
           wobj.API_SetParameter("loglevel", "true");           
           wobj.API_SetParameter("startsipstack", "1");
           
           //Initialize the sip stack
           wobj.API_Start();
           Ip = IpSet.getText() + "@192.168.0.120";
           wobj.API_Call(-1, Ip);
            HiloRecord r = new HiloRecord();
            
            r.wobj = wobj;
            
            System.out.println(Ip.substring(Ip.length()-2));
            ClientVoice.calling = true;
            r.start();
           
//Make a call (replace DESTINATION with a SIP username, extension, phone number or SIP URI)

/*
callincoming = true;
Socket sr = new Socket("192.168.43.185",8585);
DataOutputStream cont = new DataOutputStream(sr.getOutputStream());
cont.writeUTF("5"+"192.168.43."+myNm);
sr.close();

PanelAviso.setBackground(Color.green);
Label.setText("Enlazado");
Ip = incomingIp;
init_audio_get();
init_audio_set();*/

  
       
    }//GEN-LAST:event_btnStartActionPerformed

    private void btnDetieneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDetieneActionPerformed
/*if(callincomingmode){ 
    callincoming =true;
    Ip = incomingIp;
     CancelCallAck();

            }else{
                PanelAviso.setBackground(Color.red);
                Label.setText("Llamada finalizada");
                 CancelCallAck();
           //codigo para cancelar la llamda
       }
       btnStart.setVisible(true);*/
wobj.API_Hangup(-1);
//stop JVoIP when you don’t need it anymore
wobj.API_Unregister();
sipnotifications.Stop();

    }//GEN-LAST:event_btnDetieneActionPerformed


    public void CallAcepted(String ip){
        Ip = ip;
        PanelAviso.setBackground(Color.green);
        Label.setText("Llamada en curso");
        init_audio_get();
        init_audio_set();
    }
    public void CallSol(){
        //try {
            Ip = IpSet.getText() + "@192.168.43.12";
            wobj.API_Call(-1, Ip);
           // Ip = "192.168.43.12"; // +IpSet.getText();//socket solicita llamada a la otra persona
            //System.out.println(Ip);
            //Socket sr = new Socket(Ip,8500+Integer.parseInt(IpSet.getText()));
            /*Socket sr = new Socket(Ip,8500+Integer.parseInt(IpSet.getText()));
            //Socket sr = new Socket("192.168.100.12",8512);
            DataOutputStream cont = new DataOutputStream(sr.getOutputStream());
            cont.writeUTF("0"+"192.168.43."+myNm);
            sr.close();
            Runnable r = new HiloCalling(this);
            Thread t = new Thread(r);
            t.start();
            this.callincomoutgmode = true;*/
        /*} catch (IOException ex) {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
            PanelAviso.setBackground(Color.red);
            Label.setText("Linea no disponible");
            
        }*/
        
    }
    
    public void CancelCallAck(){
        
            wobj.API_Hangup(-1);
            sipnotifications.Stop();
        
       
        
    }

    public void incomng(String incomingIp){
        this.incomingIp = incomingIp;
        Runnable r = new HiloIncoming(this);
        Thread t = new Thread(r);
        t.start();
        this.callincomingmode = true;
    }
       public void init_audio_get(){
        try {
            AudioFormat format  = getaudioformat();
            DataLine.Info info_out = new DataLine.Info(SourceDataLine.class, format);
            if(!AudioSystem.isLineSupported(info_out)){
                System.out.println("Este equipo no es compatible");
                System.exit(0);
            }
            audio_out = (SourceDataLine) AudioSystem.getLine(info_out);
            audio_out.open(format);
            audio_out.start();
            HiloPlayer r = new HiloPlayer();
            //r.datain = new DatagramSocket(8509);
            r.datain = new DatagramSocket(8000+Integer.valueOf(myNm));
            r.audio_out = audio_out;
            ClientVoice.calling = true;
            r.start();
            
        } catch (LineUnavailableException | SocketException ex) {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    public void init_audio_set(){
        try {
            AudioFormat format  = getaudioformat();
            DataLine.Info info = new DataLine.Info(TargetDataLine.class,format);
            if(!AudioSystem.isLineSupported(info)){
                System.out.println("Este equipo no es compatible");
                System.exit(0);
            }
            audio_in = (TargetDataLine) AudioSystem.getLine(info);
            audio_in.open(format);
            audio_in.start();
            HiloRecord r = new HiloRecord();
            InetAddress inet  = InetAddress.getByName(Ip);
            r.audio_in = audio_in;
            r.dataOut = new DatagramSocket();
            r.server_ip = inet;
            r.serverPort = 8085;
            System.out.println(Ip.substring(Ip.length()-2));
            ClientVoice.calling = true;
            r.start();
            btnStart.setVisible(false);
            btnDetiene.setVisible(true);
        } catch (LineUnavailableException | UnknownHostException | SocketException ex) {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField IpSet;
    public javax.swing.JLabel Label;
    public javax.swing.JPanel PanelAviso;
    private javax.swing.JButton btnDetiene;
    private javax.swing.JButton btnStart;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
