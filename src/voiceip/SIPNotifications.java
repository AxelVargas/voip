/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voiceip;



import webphone.webphone;

 

 

public class SIPNotifications extends Thread

{

   boolean terminated = false;

   webphone webphoneobj = null;

  

   //constructor

   public SIPNotifications(webphone webphoneobj_in)

   {

       webphoneobj = webphoneobj_in;

   }

 

   //start the thread

   public boolean Start()

   {

 

       try{

           this.start();

           System.out.println("sip notifications started");

           return true;

       }catch(Exception e) {System.out.println("Exception at SIPNotifications Start: "+e.getMessage()+"\r\n"+e.getStackTrace()); }

       return false;

   }

 

  //stop the thread

   public void Stop()

   {

           terminated = true;

   }

 

   //blocking read in this thread

   public void run()

   {

        try{

           String sipnotifications = "";

           String[] notarray = null;

          

           while (!terminated)

           {

                  //get the notifications from the SIP stack

                  sipnotifications = webphoneobj.API_GetNotificationsSync();

 

                  if (sipnotifications != null && sipnotifications.length() > 0)

                  {

                      //split by line

                      System.out.println("\tREC FROM JVOIP: " + sipnotifications);

                      notarray = sipnotifications.split("\r\n");

 

                      if (notarray == null || notarray.length < 1)

                      {                               

                         if(!terminated) Thread.sleep(1); //some error occured. sleep a bit just to be sure to avoid busy loop

                      }

                      else

                      {

                        for (int i = 0; i < notarray.length; i++)

                        {

                            if (notarray[i] != null && notarray[i].length() > 0)

                            {                                        

                                ProcessNotifications(notarray[i]);

                            }

                        }

                      }

                  }

                  else

                  {

                      if(!terminated) Thread.sleep(1);  //some error occured. sleep a bit just to be sure to avoid busy loop

                  }                                       

           }

 

        }catch(Exception e)

        {

           if(!terminated) System.out.println("Exception at SIPNotifications run: "+e.getMessage()+"\r\n"+e.getStackTrace());

        }

 

    }

 

 

    //all messages from JVoIP will be routed to this function.

    //parse and process them after your needs regarding to your application logic

 

    public void ProcessNotifications(String msg)

    {

        try{           

            //frame.jTextArea1.append(msg);

           

            //TODO: process notifications here (change your user interface or business logic depending on the sip stack state / call state by parsing the strings receiver here).

          //See the Notifications section in the documentation for the details. Example code can be found here.

        }catch(Exception e) { System.out.println("Exception at SIPNotifications ProcessNotifications: "+e.getMessage()+"\r\n"+e.getStackTrace()); }

    }

}

 